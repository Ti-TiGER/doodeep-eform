import Login from "./theme_pages/Authentication/Login";
import Menu from "./theme_components/HorizontalLayout/index";
import Dashboard from "./theme_pages/Dashboard/index";
import FormElements from "./theme_pages/Forms/FormElements/index";
import FromLayouts from "./theme_pages/Forms/FormLayouts";
import Calendar from "./theme_pages/Calendar/index";
import Main from "./theme_pages/Eform/Mainform";
import Form1 from "./theme_pages/Eform/Form1";
import Form2 from "./theme_pages/Eform/Form2";
import Form3 from "./theme_pages/Eform/Form3";
import Form4 from "./theme_pages/Eform/Form4";
import Form5 from "./theme_pages/Eform/Form5";
import Form6 from "./theme_pages/Eform/Form6";
import EcommerceOrders from "./E-Commerce/index";
import EditableTables from "./theme_pages/Tables/EditableTables";
import UiTabsAccordions from "./theme_pages/UI/UiTabsAccordions";
import UiProgressbar from "./theme_pages/UI/UiProgressbar";
import IconBoxicons from "./theme_pages/Icons/IconBoxicons";
import IconDripicons from "./theme_pages/Icons/IconDripicons";
import IconFontawesome from "./theme_pages/Icons/IconFontawesome";
import IconMaterialdesign from "./theme_pages/Icons/IconMaterialdesign";

import Navbar from "./CRUD/Navbar";
// CRUD
import Users from "./CRUD/User";
import UserCreate from "./CRUD/UserCreate";
import UserUpdate from "./CRUD/UserUpdate";
// Login
import SignIn from "./LoginPage/Signin";
import Profile from "./LoginPage/Profile";
// Themed CRUD
import UserPage from "./ThemedCRUD/Userpage";
import Create from "./ThemedCRUD/AddUserpage";
import Update from "./ThemedCRUD/UpdateUserpage";
// Themed Login
import Header from "./ThemedLogin/index";
import ThemeLogin from "./ThemedLogin/Login";
import ThemeProfile from "./ThemedLogin/ThemedProfile";
// Etable
import Etable from "./theme_pages/Tables/Etable";
// API
import Travelapi from "./appapi/travel";
import Detail from "./appapi/Details";

import "../src/App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        {/* Theme Stuff */}
        <Route path="/pages-login">
          <Login />
        </Route>
        <Route path="/dashboard">
          <Menu />
          <Dashboard />
        </Route>
        <Route path="/calendar">
          <Menu />
          <Calendar />
        </Route>
        <Route path="/form-elements">
          <Menu />
          <FormElements />
        </Route>
        <Route path="/form-layouts">
          <Menu />
          <FromLayouts />
        </Route>

        <Route path="/ecommerce-orders">
          <Menu />
          <EcommerceOrders />
        </Route>

        <Route path="/tables-editable">
          <Menu />
          <EditableTables />
        </Route>

        <Route path="/ui-tabs-accordions">
          <Menu />
          <UiTabsAccordions />
        </Route>

        <Route path="/ui-progressbars">
          <Menu />
          <UiProgressbar />
        </Route>

        <Route path="/IconBoxicons">
          <Menu />
          <IconBoxicons />
        </Route>

        <Route path="/IconDripicons">
          <Menu />
          <IconDripicons />
        </Route>

        <Route path="/IconFontawesome">
          <Menu />
          <IconFontawesome />
        </Route>

        <Route path="/IconMaterialdesign">
          <Menu />
          <IconMaterialdesign />
        </Route>

        <Route path="/SignIn-page">
          <Menu />
          <SignIn />
        </Route>

        <Route path="/Profile-page">
          <Menu />
          <Profile />
        </Route>

        {/*  Tiger Stuff */}
        <Route path="/User-page">
          <Menu />
          <Users />
        </Route>

        <Route path="/create">
          <Menu />
          <UserCreate />
        </Route>

        <Route path="/update/:id">
          <Menu />
          <UserUpdate />
        </Route>

        <Route path="/Main-form">
          <Menu />
          <Main />
        </Route>

        <Route path="/form-page1">
          <Menu />
          <Form1 />
        </Route>

        <Route path="/form-page2">
          <Menu />
          <Form2 />
        </Route>

        <Route path="/form-page3">
          <Menu />
          <Form3 />
        </Route>

        <Route path="/form-page4">
          <Menu />
          <Form4 />
        </Route>

        <Route path="/form-page5">
          <Menu />
          <Form5 />
        </Route>

        <Route path="/form-page6">
          <Menu />
          <Form6 />
        </Route>

        <Route path="/CRUD-page">
          <Menu />
          <UserPage />
        </Route>

        <Route path="/C-page">
          <Menu />
          <Create />
        </Route>

        <Route path="/U-page/:id">
          <Menu />
          <Update />
        </Route>

        <Route path="/E-Table-page">
          <Menu />
          <Etable />
        </Route>

        <Route path="/Theme-Login-page">
          <ThemeLogin />
        </Route>

        <Route path="/Theme-Profile-page">
          <Header />
          <ThemeProfile />
        </Route>

        <Route path="/api">
          <Menu />
          <Travelapi />
        </Route>

        <Route path="/detail">
          <Menu />
          <Detail />
        </Route>

        {/* Default first page */}
        <Route path="/">
          <Menu />
          <Dashboard />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
