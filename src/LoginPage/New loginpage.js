import React, { useState, useEffect } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  Col,
  Container,
  Row,
  Label,
  FormGroup,
  Button,
  Form,
  Input,
} from "reactstrap";

// import Sweet alert
import swal from "sweetalert";

async function loginUser(credentials) {
  return fetch("https://www.mecallapi.com/api/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}

export default function Login() {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await loginUser({
      username,
      password,
    });
    if ("accessToken" in response) {
      swal("Success", response.message, "success", {
        buttons: false,
        timer: 2000,
      }).then((value) => {
        localStorage.setItem("accessToken", response["accessToken"]);
        localStorage.setItem("user", JSON.stringify(response["user"]));
        window.location.href = "/Theme-Profile-page";
      });
    } else {
      swal("Failed", response.message, "error");
    }
  };

  return (
    <React.Fragment>
      <div>
        <MetaTags>
          <title>Login - page | Skote - React Template</title>
        </MetaTags>
      </div>

      <Container fluid>
        <Card>
          <Row lg={12}>
            <Col>
              <img
                style={{ width: 1080, height: 1080 }}
                src={"https://source.unsplash.com/random"}
              />
            </Col>
            <Col style={{ paddingTop: 15 }}>
              <Form noValidate onSubmit={handleSubmit}>
                <FormGroup>
                  <Row md={6} className="d-flex justify-content-center">
                    <Col md={6} className="d-flex justify-content-center">
                      <button
                        className="d-flex justify-content-center btn btn-sm btn-grey rounded-pill"
                        style={{
                          width: "3rem",
                          height: "3rem",
                          paddingLeft: 14,
                        }}
                      >
                        <i className="bx bx-lock bx-md me-2" />
                      </button>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="d-flex justify-content-center">
                      <Label
                        style={{
                          alignItems: "center",
                          fontSize: 20,
                        }}
                      >
                        Sign In
                      </Label>
                    </Col>
                  </Row>
                  <Row
                    md={12}
                    className="position-relative d-flex justify-content-center mb-3"
                  >
                    <Col md={12}>
                      <Input
                        id="email"
                        bsSize="lg"
                        type="email"
                        className="form-control"
                        placeholder="Email Address*"
                        onChange={(e) => setUserName(e.target.value)}
                      />
                    </Col>
                  </Row>
                  {/* New Row */}
                  <Row
                    md={12}
                    className="position-relative d-flex justify-content-center mb-3"
                  >
                    <Col md={12}>
                      <Input
                        id="password"
                        bsSize="lg"
                        type="password"
                        className="form-control"
                        placeholder="Password*"
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </Col>
                  </Row>
                  {/* Button Row */}
                  <Row
                    md={2}
                    className="position-relative d-flex justify-content-center mb-3"
                  >
                    <Button
                      block
                      color="primary"
                      type="submit"
                      size="lg"
                      style={{ marginTop: 3 }}
                    >
                      Sign In
                    </Button>
                  </Row>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Card>
      </Container>
    </React.Fragment>
  );
}