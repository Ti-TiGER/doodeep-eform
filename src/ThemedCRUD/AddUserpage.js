import React, { useState } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  Col,
  Container,
  Row,
  Label,
  Form,
  FormGroup,
  Input,
  Button,
} from "reactstrap";

export default function UserCreate() {
  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      fname: fname,
      lname: lname,
      username: username,
      email: email,
      avatar: avatar,
    };
    fetch("https://www.mecallapi.com/api/users/create", {
      method: "POST",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          window.location.href = "/E-Table-page";
        }
      });
  };

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");
  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>Create User - page | Skote - React Template</title>
        </MetaTags>
      </div>
      <Container md={12} onSubmit={handleSubmit}>
        <Container md={12} className="d-flex justify-content-center">
          <Label style={{ color: "#546ce4", fontSize: 24 }}>
            Creating User
          </Label>
        </Container>
        <Container md={6}>
          <Card md={6}>
            <Form inline>
              <FormGroup floating>
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={3}>
                    <Input
                      id="FirstName"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      placeholder="First Name*"
                      onChange={(e) => setFname(e.target.value)}
                    />
                  </Col>
                  <Col md={3}>
                    <Input
                      id="LastName"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      placeholder="Last Name*"
                      onChange={(e) => setLname(e.target.value)}
                    />
                  </Col>
                </Row>
                {/* New Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={6}>
                    <Input
                      id="Username"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      placeholder="Username*"
                      onChange={(e) => setUsername(e.target.value)}
                    />
                  </Col>
                </Row>
                {/* New Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={6}>
                    <Input
                      id="Email"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      placeholder="Email*"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Col>
                </Row>
                {/* New Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={6}>
                    <Input
                      id="Avartar"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      placeholder="Avartar*"
                      onChange={(e) => setAvatar(e.target.value)}
                    />
                  </Col>
                </Row>
                {/* Button Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Button
                    type="submit"
                    block
                    color="primary"
                    size="lg"
                    style={{ marginTop: 3 }}
                  >
                    Create
                  </Button>
                </Row>
              </FormGroup>
            </Form>
          </Card>
        </Container>
      </Container>
    </React.Fragment>
  );
}
