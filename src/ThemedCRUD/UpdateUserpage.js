import React, { useState, useEffect } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  Col,
  Container,
  Row,
  Label,
  Form,
  FormGroup,
  Input,
  Button,
} from "reactstrap";

import { useParams } from "react-router-dom";

export default function UpdateUser() {
  const { id } = useParams();
  useEffect(() => {
    fetch("https://www.mecallapi.com/api/users/" + id)
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        setAvatar(result.user.avatar);
        setFname(result.user.fname);
        setLname(result.user.lname);
        setUsername(result.user.username);
        setEmail(result.user.email);
      });
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      id: id,
      fname: fname,
      lname: lname,
      username: username,
      email: email,
      avatar: avatar,
    };
    fetch("https://www.mecallapi.com/api/users/update", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          window.location.href = "/E-Table-page";
        }
      });
  };

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");

  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>Update User page | Skote - React Template</title>
        </MetaTags>
      </div>
      <Container md={12}>
        <Container md={12} className="d-flex justify-content-center">
          <Label style={{ color: "#546ce4", fontSize: 24 }}>
            Update User Info
          </Label>
        </Container>
        <Container md={6} onSubmit={handleSubmit}>
          <Card md={6}>
            <Form inline>
              <FormGroup floating>
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={3}>
                    <Input
                      id="FirstName"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      value={fname}
                      onChange={(e) => setFname(e.target.value)}
                    />
                  </Col>
                  <Col md={3}>
                    <Input
                      id="LastName"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      value={lname}
                      onChange={(e) => setLname(e.target.value)}
                    />
                  </Col>
                </Row>

                {/* New Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={6}>
                    <Input
                      id="Email"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Col>
                </Row>
                {/* New Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Col md={6}>
                    <Input
                      id="Avartar"
                      bsSize="lg"
                      type="text"
                      className="form-control"
                      value={avatar}
                      onChange={(e) => setAvatar(e.target.value)}
                    />
                  </Col>
                </Row>
                {/* Button Row */}
                <Row
                  md={6}
                  className="position-relative d-flex justify-content-center mb-3"
                >
                  <Button
                    type="submit"
                    block
                    color="primary"
                    size="lg"
                    style={{ marginTop: 3 }}
                  >
                    Update
                  </Button>
                </Row>
              </FormGroup>
            </Form>
          </Card>
        </Container>
      </Container>
    </React.Fragment>
  );
}
