import React, { useState, useEffect } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  Col,
  Container,
  Row,
  Label,
  FormGroup,
  Button,
} from "reactstrap";
import { Link } from "react-router-dom";

// Editable
import BootstrapTable from "react-bootstrap-table-next";

export default function Userpage() {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    UsersGet();
  }, []);

  const UsersGet = () => {
    fetch("https://www.mecallapi.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        var data = [];
        for (let user of result) {
          data.push({
            ID: user.id,
            Avatar: (
              <div className="avatar-sm ">
                <img
                  src={user.avatar}
                  alt=""
                  className="img-thumbnail rounded-circle"
                />
              </div>
            ),
            FirstName: user.fname,
            LastName: user.lname,
            Email: user.username,
            Action: (
              <FormGroup className="d-flex justify-content-center">
                <Button
                  onClick={() => UpdateUser(user.id)}
                  style={{ marginRight: 15 }}
                >
                  <i className="mdi mdi-pencil font-size-18" />
                </Button>
                <Button onClick={() => UserDelete(user.id)}>
                  <i className="mdi mdi-delete font-size-18" />
                </Button>
              </FormGroup>
            ),
          });
        }
        setUsers(data);
        console.log(data);
      });
  };

  const UpdateUser = (id) => {
    window.location = "/U-page/" + id;
  };

  const UserDelete = (id) => {
    var data = {
      id: id,
    };
    fetch("https://www.mecallapi.com/api/users/delete", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          UsersGet();
        }
      });
  };

  const columns = [
    {
      dataField: "ID",
      text: "ID",
      headerAlign: "center",
      headerStyle: {
        backgroundColor: "#682e77",
        color: "#fff",
      },
    },

    {
      dataField: "Avatar",
      text: "Avatar",
      headerAlign: "center",
      headerStyle: {
        backgroundColor: "#682e77",
        color: "#fff",
      },
    },

    {
      dataField: "FirstName",
      text: "FirstName",
      headerAlign: "center",
      headerStyle: { backgroundColor: "#682e77", color: "#fff" },
    },

    {
      dataField: "LastName",
      text: "LastName",
      headerAlign: "center",
      headerStyle: { backgroundColor: "#682e77", color: "#fff" },
    },

    {
      dataField: "Email",
      text: "Email",
      headerAlign: "center",
      headerStyle: {
        backgroundColor: "#682e77",
        color: "#fff",
      },
    },

    {
      dataField: "Action",
      text: "Action",
      headerAlign: "center",
      headerStyle: {
        backgroundColor: "#682e77",
        color: "#fff",
      },
    },
  ];

  const rowStyle2 = (row, rowIndex) => {
    const style = {};
    style.backgroundColor = "#ccc";
    style.fontWeight = "bold";
    style.color = "#000000";

    return style;
  };

  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>User - page | Skote - React Template</title>
        </MetaTags>
      </div>

      <Container style={{ marginBottom: 10 }}>
        <Row md={12}>
          <Col className="d-flex justify-content-start">
            <Label style={{ color: "#546ce4", fontSize: 18 }}>USERS</Label>
          </Col>
          <Col className="d-flex justify-content-end">
            <Link to="/C-page">
              <Button color="primary">CREATE</Button>
            </Link>
          </Col>
        </Row>
      </Container>

      {/* Table */}

      <Container>
        <Card>
          <Row md={12}>
            <Col md={12}>
              <div className="table-responsive">
                <BootstrapTable
                  keyField="ID"
                  data={users}
                  columns={columns}
                  rowStyle={rowStyle2}
                />
              </div>
            </Col>
          </Row>
        </Card>
      </Container>
    </React.Fragment>
  );
}
