import React, { Component } from "react";
import { Row, Col, Collapse } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import classname from "classnames";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    let matchingMenuItem = null;
    const ul = document.getElementById("navigation");
    const items = ul.getElementsByTagName("a");
    for (let i = 0; i < items.length; ++i) {
      if (this.props.location.pathname === items[i].pathname) {
        matchingMenuItem = items[i];
        break;
      }
    }
    if (matchingMenuItem) {
      this.activateParentDropdown(matchingMenuItem);
    }
  }

  activateParentDropdown = (item) => {
    item.classList.add("active");
    const parent = item.parentElement;
    if (parent) {
      parent.classList.add("active"); // li
      const parent2 = parent.parentElement;
      parent2.classList.add("active"); // li
      const parent3 = parent2.parentElement;
      if (parent3) {
        parent3.classList.add("active"); // li
        const parent4 = parent3.parentElement;
        if (parent4) {
          parent4.classList.add("active"); // li
          const parent5 = parent4.parentElement;
          if (parent5) {
            parent5.classList.add("active"); // li
            const parent6 = parent5.parentElement;
            if (parent6) {
              parent6.classList.add("active"); // li
            }
          }
        }
      }
    }
    return false;
  };

  render() {
    return (
      <React.Fragment>
        <div className="topnav" style={{ marginTop: 70 }}>
          <div className="container-fluid">
            <nav
              className="navbar navbar-light navbar-expand-lg topnav-menu"
              id="navigation"
            >
              <Collapse
                isOpen={this.props.menuOpen}
                className="navbar-collapse"
                id="topnav-menu-content"
              >
                <ul className="navbar-nav">
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({ isDashboard: !this.state.isDashboard });
                      }}
                      to="/dashboard"
                    >
                      <i className="bx bx-home-circle me-2" />
                      {"Home"} {this.props.menuOpen}
                      <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.isDashboard,
                      })}
                    >
                      <Link to="/dashboard" className="dropdown-item">
                        {"Default"}
                      </Link>
                    </div>
                  </li>

                  {/* อันที่ 4 */}

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({
                          login: !this.state.login,
                        });
                      }}
                    >
                      <i className="bx bx-user-circle me-2" />
                      {"Login page (Original)"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.login,
                      })}
                    >
                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              formState: !this.state.login,
                            });
                          }}
                        >
                          <i className="bx bx-user me-2" />
                          {"Orginal page"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.login,
                          })}
                        >
                          <Link to="/SignIn-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Login page"}
                          </Link>

                          <Link to="/Profile-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Profile page"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </li>

                  {/* Two */}

                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({ Themedlogin: !this.state.Themedlogin });
                      }}
                      to="/dashboard"
                    >
                      <i className="bx bx-user me-2" />
                      {"Themed Login page"} {this.props.menuOpen}
                      <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.Themedlogin,
                      })}
                    >
                      <Link to="/Theme-Login-page" className="dropdown-item">
                        <i className="bx bx-lock me-2" />
                        {"ThemedLogin-Page"}
                      </Link>
                      <Link to="/Theme-Profile-page" className="dropdown-item">
                        <i className="bx bx-user-check me-2" />
                        {"ThemedProfile-Page"}
                      </Link>
                    </div>
                  </li>
                </ul>
              </Collapse>
            </nav>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(Navbar);
