import React, { useState, useEffect } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  Col,
  Container,
  Row,
  Label,
  CardBody,
  FormGroup,
  Button,
  Form,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";

import { Link } from "react-router-dom";
import Tiger from "../../src/theme_components/Image/Tiger.png";

// import Sweet alert
import swal from "sweetalert";

// import

export default function Header() {
  const [socialDrp, setsocialDrp] = React.useState();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const user = JSON.parse(localStorage.getItem("user"));

  const handleLogout = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("user");
    window.location.href = "/Theme-Login-page";
  };

  return (
    <React.Fragment>
      <header id="page-topbar">
        <Row
          lg={12}
          style={{
            cursor: "pointer",
            backgroundColor: "#396EB0",
            color: "#fff",
            fontSize: 16,
          }}
        >
          <Col>
            <div className="navbar-brand-box d-flex justify-content-start">
              <Link to="/form-page1" className="logo logo-light ml-logo">
                <span className="logo-md">
                  <img src={Tiger} alt="" height="55" />
                </span>
              </Link>
            </div>
          </Col>

          <Col className=" d-flex justify-content-end">
            <Dropdown
              className="d-none d-lg-inline-block ms-1"
              isOpen={socialDrp}
              toggle={() => {
                setsocialDrp({ socialDrp: !socialDrp });
              }}
            >
              <DropdownToggle
                className="btn header-item noti-icon"
                caret
                tag="button"
              >
                <Col>
                  <Row>
                    <Col>
                      <img
                        src={user.avatar}
                        className=" avatar-sm img rounded-circle"
                      />
                    </Col>
                    <Col>
                      <Label
                        className="font-size"
                        style={{
                          color: "#fff",
                          paddingRight: 50,
                          paddingTop: 13,
                        }}
                      >
                        {user.fname} {user.lname}
                      </Label>
                    </Col>
                  </Row>
                </Col>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-sm dropdown-menu-end">
                <div className="px-sm-2">
                  <Row className="no-gutters">
                    <Col className="d-flex justify-content-center">
                      <Button outline color="danger" onClick={handleLogout}>
                        <div className="avatar-sm ">
                          <img
                            src={user.avatar}
                            className="img-thumbnail rounded-circle"
                          />
                        </div>
                        <Label
                          style={{
                            alignItems: "center",
                            fontSize: 16,
                          }}
                        >
                          Logout
                        </Label>
                      </Button>
                    </Col>
                  </Row>
                </div>
              </DropdownMenu>
            </Dropdown>
          </Col>
        </Row>
      </header>
    </React.Fragment>
  );
}
