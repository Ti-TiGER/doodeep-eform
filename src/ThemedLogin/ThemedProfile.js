import React from "react";
import MetaTags from "react-meta-tags";
import { Card, Col, Container, Row, Label } from "reactstrap";

export default function Login() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const user = JSON.parse(localStorage.getItem("user"));

  const handleLogout = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("user");
    window.location.href = "/Theme-Login-page";
  };

  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>Profile - page | Skote - React Template</title>
        </MetaTags>
      </div>

      <Container>
        <Row lg={6} className="d-flex justify-content-center"></Row>
        <Card>
          <Row lg={12} className="d-flex justify-content-center">
            <Col lg={2} className="d-flex justify-content-end">
              <div className="avatar-lg ">
                <img
                  src={user.avatar}
                  className="img-thumbnail rounded-circle"
                />
              </div>
            </Col>
            <Col lg={6} className="d-flex justify-content-start">
              <Label
                style={{
                  alignItems: "center",
                  fontSize: 30,
                  paddingTop: 25,
                }}
              >
                Welcome {user.fname} {user.lname}
              </Label>
            </Col>
          </Row>
        </Card>
      </Container>
    </React.Fragment>
  );
}
