import React, { useState, useEffect } from "react";
import {
  Card,
  Col,
  Container,
  Row,
  Button,
  CardBody,
  CardTitle,
  CardText,
  CardImg,
} from "reactstrap";

import Breadcrumbs from "../theme_components/Common/Breadcrumb";
import MetaTags from "react-meta-tags";
import { Link } from "react-router-dom";

export default function Details() {
  const [attractions, setAttractions] = useState([]);

  useEffect(() => {
    fetch("https://www.mecallapi.com/api/attractions")
      .then((res) => res.json())
      .then((result) => {
        setAttractions(result);
      });
  }, []);
  return (
    <React.Fragment>
      <div>
        <MetaTags>
          <title>Full detail page | Skote - React Template</title>
        </MetaTags>
      </div>
      <Container className="mt">
        <Breadcrumbs title="API" breadcrumbItem="Travel" />
        <Container>
          <Row>
            <Col
              className="d-flex justify-content-end"
              style={{ marginBottom: 10 }}
            >
              <Link to="/api">
                <Button color="primary" size="lg">
                  see less
                </Button>
              </Link>
            </Col>
          </Row>
        </Container>
        <Row className="d-flex justify-content-center">
          {attractions.map((attraction) => (
            <Col lg={6} md={6} sm={6} xs={12} key={attraction.id}>
              <Card style={{ width: "100%" }}>
                <img
                  className="rounded"
                  variant="top"
                  src={attraction.coverimage}
                  style={{ width: "100%" }}
                />
                <CardBody>
                  <CardTitle>{attraction.name}</CardTitle>
                  <CardText>{attraction.detail}</CardText>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </React.Fragment>
  );
}
