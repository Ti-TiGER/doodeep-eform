import React, { useState, useEffect } from "react";

import {
  Card,
  Col,
  Container,
  Row,
  Button,
  CardBody,
  CardTitle,
  CardText,
  CardImg,
} from "reactstrap";
import Breadcrumbs from "../theme_components/Common/Breadcrumb";
import MetaTags from "react-meta-tags";
import { Link } from "react-router-dom";

export default function Travel() {
  const [attractions, setAttractions] = useState([]);

  useEffect(() => {
    fetch("https://www.mecallapi.com/api/attractions")
      .then((res) => res.json())
      .then((result) => {
        setAttractions(result);
      });
  }, []);
  return (
    <React.Fragment>
      <div>
        <MetaTags>
          <title>Travel page | Skote - React Template</title>
        </MetaTags>
      </div>
      <Container style={{ paddingTop: 145 }}>
        <Row>
          <Breadcrumbs title="API" breadcrumbItem="Travel" />
          {attractions.map((attraction) => (
            <Col lg={4} md={4} sm={4} xs={12} key={attraction.id}>
              <Card style={{ width: "100%" }}>
                <img
                  className="rounded"
                  variant="top"
                  src={attraction.coverimage}
                />
                <CardBody>
                  <CardTitle>{attraction.name}</CardTitle>
                  <CardText className="text-truncate">
                    {attraction.detail}
                  </CardText>
                  <Link to="/detail">
                    <Button color="primary">see more</Button>
                  </Link>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </React.Fragment>
  );
}
