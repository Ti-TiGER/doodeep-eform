import React, { Component } from "react";
import { Row, Col, Collapse } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import classname from "classnames";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    let matchingMenuItem = null;
    const ul = document.getElementById("navigation");
    const items = ul.getElementsByTagName("a");
    for (let i = 0; i < items.length; ++i) {
      if (this.props.location.pathname === items[i].pathname) {
        matchingMenuItem = items[i];
        break;
      }
    }
    if (matchingMenuItem) {
      this.activateParentDropdown(matchingMenuItem);
    }
  }

  activateParentDropdown = (item) => {
    item.classList.add("active");
    const parent = item.parentElement;
    if (parent) {
      parent.classList.add("active"); // li
      const parent2 = parent.parentElement;
      parent2.classList.add("active"); // li
      const parent3 = parent2.parentElement;
      if (parent3) {
        parent3.classList.add("active"); // li
        const parent4 = parent3.parentElement;
        if (parent4) {
          parent4.classList.add("active"); // li
          const parent5 = parent4.parentElement;
          if (parent5) {
            parent5.classList.add("active"); // li
            const parent6 = parent5.parentElement;
            if (parent6) {
              parent6.classList.add("active"); // li
            }
          }
        }
      }
    }
    return false;
  };

  render() {
    return (
      <React.Fragment>
        <div className="topnav">
          <div className="container-fluid">
            <nav
              className="navbar navbar-light navbar-expand-lg topnav-menu"
              id="navigation"
            >
              <Collapse
                isOpen={this.props.menuOpen}
                className="navbar-collapse"
                id="topnav-menu-content"
              >
                <ul className="navbar-nav">
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({ isDashboard: !this.state.isDashboard });
                      }}
                      to="/dashboard"
                    >
                      <i className="bx bx-home-circle me-2" />
                      {"Dashboard"} {this.props.menuOpen}
                      <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.isDashboard,
                      })}
                    >
                      <Link to="/dashboard" className="dropdown-item">
                        {"Default"}
                      </Link>
                      <Link to="/api" className="dropdown-item">
                        {"API"}
                      </Link>
                    </div>
                  </li>

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({ uiState: !this.state.uiState });
                      }}
                      className="nav-link dropdown-toggle arrow-none"
                    >
                      <i className="bx bx-tone me-2" />
                      {"UI Elements & Icons"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname(
                        "dropdown-menu mega-dropdown-menu dropdown-menu-left dropdown-mega-menu-md",
                        { show: this.state.uiState }
                      )}
                    >
                      <Row>
                        <Col lg={4}>
                          <div>
                            <Link
                              to="/ui-progressbars"
                              className="dropdown-item"
                            >
                              {"Progress Bars"}
                            </Link>
                            <Link
                              to="/ui-tabs-accordions"
                              className="dropdown-item"
                            >
                              {"Tabs & Accordions"}
                            </Link>
                            <Link to="/IconBoxicons" className="dropdown-item">
                              {"IconBoxicons"}
                            </Link>
                            <Link to="/IconDripicons" className="dropdown-item">
                              {"IconDripicons"}
                            </Link>
                            <Link
                              to="/IconFontawesome"
                              className="dropdown-item"
                            >
                              {"IconFontawesome"}
                            </Link>
                            <Link
                              to="/IconMaterialdesign"
                              className="dropdown-item"
                            >
                              {"IconMaterialdesign"}
                            </Link>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </li>

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({
                          componentState: !this.state.componentState,
                        });
                      }}
                    >
                      <i className="bx bx-collection me-2" />
                      {"Components"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.componentState,
                      })}
                    >
                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({ formState: !this.state.formState });
                          }}
                        >
                          {"Forms"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.formState,
                          })}
                        >
                          <Link to="/form-elements" className="dropdown-item">
                            {"Form Elements"}
                          </Link>
                          <Link to="/form-layouts" className="dropdown-item">
                            {"Form Layouts"}
                          </Link>
                        </div>
                      </div>

                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              tableState: !this.state.tableState,
                            });
                          }}
                        >
                          {"Tables"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.tableState,
                          })}
                        >
                          <Link to="/tables-editable" className="dropdown-item">
                            {"Editable Table"}
                          </Link>

                          <Link
                            to="/ecommerce-orders"
                            className="dropdown-item"
                          >
                            {"Orders"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </li>

                  {/* ขั่นอันใหม่ */}

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({
                          eformState: !this.state.eformState,
                        });
                      }}
                    >
                      <i className="bx bxs-factory me-2" />
                      {"Eform"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.eformState,
                      })}
                    >
                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              formState: !this.state.eformState,
                            });
                          }}
                        >
                          {"EForms"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.eformState,
                          })}
                        >
                          <Link to="/Main-form" className="dropdown-item">
                            {"Main Form"}
                          </Link>
                          <Link to="/form-page1" className="dropdown-item">
                            {"Form 1"}
                          </Link>
                          <Link to="/form-page2" className="dropdown-item">
                            {"Form 2"}
                          </Link>
                          <Link to="/form-page3" className="dropdown-item">
                            {"Form 3"}
                          </Link>
                          <Link to="/form-page4" className="dropdown-item">
                            {"Form 4"}
                          </Link>
                          <Link to="/form-page5" className="dropdown-item">
                            {"Form 5"}
                          </Link>
                          <Link to="/form-page6" className="dropdown-item">
                            {"Form 6"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </li>

                  {/* อันที่ 2 */}

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({
                          calendar: !this.state.calendar,
                        });
                      }}
                    >
                      <i className="bx bx-calendar me-2" />
                      {"Calerdar"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.calendar,
                      })}
                    >
                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              formState: !this.state.calendar,
                            });
                          }}
                        >
                          <i className="bx bx-calendar me-2" />
                          {"Calerdar"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.calendar,
                          })}
                        >
                          <Link to="/calendar" className="dropdown-item">
                            <i className="bx bx-calendar me-2" />
                            {"Calendar"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </li>

                  {/* อันที่ 3 */}

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({
                          crud: !this.state.crud,
                        });
                      }}
                    >
                      <i className="bx bx-cube-alt me-2" />
                      {"CRUD page"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.crud,
                      })}
                    >
                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              formState: !this.state.crud,
                            });
                          }}
                        >
                          <i className="bx bx-polygon me-2" />
                          {"CRUD page"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.crud,
                          })}
                        >
                          <Link to="/User-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"User page"}
                          </Link>
                        </div>
                      </div>

                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              newcrud: !this.state.newcrud,
                            });
                          }}
                        >
                          <i className="bx bx-polygon me-2" />
                          {"New CRUD page"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.newcrud,
                          })}
                        >
                          <Link to="/CRUD-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Themed User page"}
                          </Link>

                          <Link to="/C-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Create-User"}
                          </Link>

                          <Link to="/U-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Update-User"}
                          </Link>
                        </div>
                      </div>

                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              Themedcrud: !this.state.Themedcrud,
                            });
                          }}
                        >
                          <i className="bx bx-polygon me-2" />
                          {"Themed CRUD page"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.Themedcrud,
                          })}
                        >
                          <Link to="/E-Table-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Updated Table-CRUD"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </li>

                  {/* อันที่ 4 */}

                  <li className="nav-item dropdown">
                    <Link
                      to="/#"
                      className="nav-link dropdown-toggle arrow-none"
                      onClick={(e) => {
                        e.preventDefault();
                        this.setState({
                          login: !this.state.login,
                        });
                      }}
                    >
                      <i className="bx bx-user-circle me-2" />
                      {"Login page"} <div className="arrow-down" />
                    </Link>
                    <div
                      className={classname("dropdown-menu", {
                        show: this.state.login,
                      })}
                    >
                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              formState: !this.state.login,
                            });
                          }}
                        >
                          <i className="bx bx-user me-2" />
                          {"Orginal Login page"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.login,
                          })}
                        >
                          <Link to="/SignIn-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Login page"}
                          </Link>

                          <Link to="/Profile-page" className="dropdown-item">
                            <i className="bx bx-right-arrow-alt me-2" />
                            {"Profile page"}
                          </Link>
                        </div>
                      </div>

                      <div className="dropdown">
                        <Link
                          to="/#"
                          className="dropdown-item dropdown-toggle arrow-none"
                          onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                              Themedlogin: !this.state.Themedlogin,
                            });
                          }}
                        >
                          <i className="bx bx-user me-2" />
                          {"Themed Login page"} <div className="arrow-down" />
                        </Link>
                        <div
                          className={classname("dropdown-menu", {
                            show: this.state.Themedlogin,
                          })}
                        >
                          <Link
                            to="/Theme-Login-page"
                            className="dropdown-item"
                          >
                            <i className="bx bx-lock me-2" />
                            {"ThemedLogin-Page"}
                          </Link>
                          <Link
                            to="/Theme-Profile-page"
                            className="dropdown-item"
                          >
                            <i className="bx bx-user-check me-2" />
                            {"ThemedProfile-Page"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </Collapse>
            </nav>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(Navbar);
