import React, { Component, useState, useEffect } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  CardBody,
  CardText,
  CardTitle,
  Col,
  Collapse,
  Container,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  Label,
  Form,
  FormGroup,
  Input,
  Progress,
  Button,
} from "reactstrap";
import { Link } from "react-router-dom";

import Days from "../../../src/theme_components/Common/Date";
import f2building1 from "../../theme_components/Icon/f2building1.png";
import f2building2 from "../../theme_components/Icon/f2building2.png";

//Import Breadcrumb
import Breadcrumbs from "../../theme_components/Common/Breadcrumb";

class UiTabsAccordions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      activeTab1: "5",
      activeTab2: "9",
      activeTab3: "13",
      verticalActiveTab: "1",
      verticalActiveTabWithIcon: "1",
      customActiveTab: "1",
      customIconActiveTab: "1",
      activeTabJustify: "5",
      col1: true,
      col2: false,
      col3: false,
      col5: true,
      col6: true,
      col7: true,
      col8: true,
      col9: true,
      col10: false,
      col11: false,
    };
    this.toggle = this.toggle.bind(this);
    this.toggle1 = this.toggle1.bind(this);

    this.t_col1 = this.t_col1.bind(this);
    this.t_col2 = this.t_col2.bind(this);
    this.t_col3 = this.t_col3.bind(this);
    this.t_col5 = this.t_col5.bind(this);
    this.t_col6 = this.t_col6.bind(this);
    this.t_col7 = this.t_col7.bind(this);
    this.t_col8 = this.t_col8.bind(this);
    this.t_col9 = this.t_col9.bind(this);
    this.t_col10 = this.t_col10.bind(this);
    this.t_col11 = this.t_col11.bind(this);

    this.toggle2 = this.toggle2.bind(this);
    this.toggle3 = this.toggle3.bind(this);

    this.toggleVertical = this.toggleVertical.bind(this);
    this.toggleVerticalIcon = this.toggleVerticalIcon.bind(this);
    this.toggleCustom = this.toggleCustom.bind(this);
    this.toggleIconCustom = this.toggleIconCustom.bind(this);
  }

  t_col1() {
    this.setState({
      col1: !this.state.col1,
    });
  }

  t_col2() {
    this.setState({
      col2: !this.state.col2,
    });
  }

  t_col3() {
    this.setState({
      col3: !this.state.col3,
    });
  }

  t_col5() {
    this.setState({ col5: !this.state.col5 });
  }

  t_col6() {
    this.setState({ col6: !this.state.col6 });
  }

  t_col7() {
    this.setState({ col7: !this.state.col7 });
  }

  t_col8() {
    this.setState({
      col6: !this.state.col6,
      col7: !this.state.col7,
    });
  }

  t_col9() {
    this.setState({
      col9: !this.state.col9,
      col10: false,
      col11: false,
    });
  }

  t_col10() {
    this.setState({
      col10: !this.state.col10,
      col9: false,
      col11: false,
    });
  }

  t_col11() {
    this.setState({
      col11: !this.state.col11,
      col9: false,
      col10: false,
    });
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggle1(tab) {
    if (this.state.activeTab1 !== tab) {
      this.setState({
        activeTab1: tab,
      });
    }
  }

  toggle2(tab) {
    if (this.state.activeTab2 !== tab) {
      this.setState({
        activeTab2: tab,
      });
    }
  }

  toggle3(tab) {
    if (this.state.activeTab3 !== tab) {
      this.setState({
        activeTab3: tab,
      });
    }
  }

  toggleVertical(tab) {
    if (this.state.verticalActiveTab !== tab) {
      this.setState({
        verticalActiveTab: tab,
      });
    }
  }

  toggleVerticalIcon(tab) {
    if (this.state.verticalActiveTabWithIcon !== tab) {
      this.setState({
        verticalActiveTabWithIcon: tab,
      });
    }
  }

  toggleCustom(tab) {
    if (this.state.customActiveTab !== tab) {
      this.setState({
        customActiveTab: tab,
      });
    }
  }

  toggleIconCustom(tab) {
    if (this.state.customIconActiveTab !== tab) {
      this.setState({
        customIconActiveTab: tab,
      });
    }
  }

  render() {
    const products = [
      { id: 1, age: 25, qty: 1500, cost: 1000 },
      { id: 2, age: 34, qty: 1900, cost: 1300 },
      { id: 3, age: 67, qty: 1300, cost: 1300 },
      { id: 4, age: 23, qty: 1100, cost: 6400 },
      { id: 5, age: 78, qty: 1400, cost: 4000 },
    ];

    const columns = [
      {
        dataField: "id",
        text: "ID",
      },
      {
        dataField: "age",
        text: "Age(AutoFill)",
      },
      {
        dataField: "qty",
        text: "Qty(AutoFill and Editable)",
      },
      {
        dataField: "cost",
        text: "Cost(Editable)",
      },
    ];

    return (
      <React.Fragment>
        <div className="page-content">
          <MetaTags>
            <title>
              Eform - form3 | Skote - React Admin & Dashboard Template
            </title>
          </MetaTags>

          <Container md={12}>
            <Row md={12}>
              <Col md={10} className="d-flex justify-content-between">
                <Label
                  style={{
                    fontSize: 20,
                    fontWeight: "bolder",
                  }}
                >
                  รายงานผลการตรวจและพิจารณาการขอรับใบอนุญาตขยายโรงงาน
                </Label>
              </Col>
              <Col md={2} className="d-flex justify-content-end">
                <Link to="/" className="back">
                  <i
                    className="bx bx-left-arrow-alt bx-md me-2 "
                    style={{ color: "#A7A7A7" }}
                  />
                </Link>
                <Link to="/" className="back">
                  <span>
                    <Label
                      style={{
                        paddingTop: 1,
                        fontSize: 20,
                        color: "#A7A7A7",
                      }}
                    >
                      ย้อนกลับ
                    </Label>
                  </span>
                </Link>
              </Col>
            </Row>
          </Container>

          <Container
            md={12}
            className=" d-flex "
            style={{ paddingTop: 40, marginBottom: 50 }}
          >
            <Col md="12" md={{ size: 6, offset: 3 }}>
              <div className="mt-5 d-flex justify-content-center">
                <Row md={12} className="position-relative ">
                  <Progress
                    max="6"
                    value={1}
                    color="success"
                    className="clr-line"
                    style={{ height: "3px" }}
                  ></Progress>
                  <Col md={2}>
                    <Link to="/form-page1">
                      <button
                        className="position-relative top-0 progress-mr translate-middle btn btn-sm btn-green rounded-pill"
                        style={{ width: "2.2rem", height: "2.2rem" }}
                      >
                        1
                      </button>
                    </Link>
                  </Col>
                  <Col md={2}>
                    <Link to="/form-page2">
                      <button
                        className="position-relative top-0 progress-mr translate-middle btn btn-sm btn-violet rounded-pill"
                        style={{ width: "2.2rem", height: "2.2rem" }}
                      >
                        2
                      </button>
                    </Link>
                  </Col>
                  <Col md={2}>
                    <Link to="/form-page3">
                      <button
                        className="position-relatives top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                        style={{ width: "2.2rem", height: "2.2rem" }}
                      >
                        3
                      </button>
                    </Link>
                  </Col>
                  <Col md={2}>
                    <Link to="/form-page4">
                      <button
                        className="position-relative top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                        style={{ width: "2.2rem", height: "2.2rem" }}
                      >
                        4
                      </button>
                    </Link>
                  </Col>
                  <Col md={2}>
                    <Link to="/form-page5">
                      <button
                        className="position-relatives top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                        style={{ width: "2.2rem", height: "2.2rem" }}
                      >
                        5
                      </button>
                    </Link>
                  </Col>
                  <Col md={2}>
                    <Link to="/form-page6">
                      <button
                        className="position-relatives top-0  translate-middle btn btn-sm btn-grey rounded-pill"
                        style={{ width: "2.2rem", height: "2.2rem" }}
                      >
                        6
                      </button>
                    </Link>
                  </Col>
                </Row>
              </div>
            </Col>
          </Container>

          {/* Ending Progress Bar */}

          <Container md={12}>
            <Card>
              <Row>
                <Col md={12} style={{ marginBottom: 30 }}>
                  <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                    <div className="d-flex justify-content-between">
                      <div>
                        <Label
                          style={{
                            textAlign: "start",
                            fontSize: 18,
                            color: "#717171",
                            paddingLeft: 20,
                          }}
                        >
                          Form 2
                        </Label>
                      </div>
                      <div>
                        <Label
                          style={{
                            fontSize: 18,
                            color: "#717171",
                            paddingRight: 20,
                          }}
                        >
                          วันที่ตรวจ <Days />
                        </Label>
                      </div>
                    </div>
                    <Container className="d-flex jusstify-content-center">
                      <Col
                        md={12}
                        style={{
                          textAlign: "center",
                          fontSize: 25,
                          fontWeight: "bold",
                        }}
                      >
                        ลักษณะอาคารและบริเวณภายในโรงงาน
                      </Col>
                    </Container>
                  </Row>
                  <div className="accordion" id="accordion">
                    <div className="accordion-item">
                      <h2 className="accordion-header" id="headingOne">
                        <button
                          className="accordion-button fw-medium acc-padding"
                          type="button"
                          onClick={this.t_col1}
                          style={{
                            cursor: "pointer",
                            backgroundColor: "#682e77",
                            color: "#fff",
                            fontSize: 16,
                          }}
                        >
                          <img
                            className="img-responsive me-4"
                            src={f2building1}
                            alt="Icon"
                          />
                          ข้อมูลทั่วไป
                        </button>
                      </h2>
                      <Collapse
                        isOpen={this.state.col1}
                        className="accordion-collapse"
                      >
                        <div className="accordion-body">
                          <Form>
                            <Label
                              style={{
                                fontSize: 16,
                                alignItems: "center",
                                fontWeight: "bolder",
                                paddingLeft: 24,
                              }}
                            >
                              อาคาร
                            </Label>

                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={12}>
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    style={{ fontSize: 14, marginLeft: 20 }}
                                  >
                                    เป็นไปตามสภาพที่ได้รับอนุญาตให้ประกอบกิจการโรงงาน
                                  </Label>
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    style={{ fontSize: 14, marginLeft: 20 }}
                                  >
                                    มีการเปลี่ยนแปลง
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    style={{ fontSize: 14, marginLeft: 20 }}
                                  >
                                    เพิ่มอาคาร
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    ลักษณะ
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                          </Form>
                        </div>
                      </Collapse>
                    </div>
                  </div>
                </Col>

                {/* Accordion ที่ 2 */}

                <Col md={12} style={{ marginBottom: 400 }}>
                  <div className="accordion" id="accordion">
                    <div className="accordion-item">
                      <h2 className="accordion-header" id="headingTwo">
                        <button
                          className="accordion-button fw-medium acc-padding"
                          type="button"
                          onClick={this.t_col2}
                          style={{
                            cursor: "pointer",
                            backgroundColor: "#682e77",
                            color: "#fff",
                            fontSize: 16,
                          }}
                        >
                          <img
                            className="img-responsive me-3"
                            src={f2building2}
                            alt="Icon"
                          />
                          เอกสารประกอบคำขอ
                        </button>
                      </h2>
                      <Collapse
                        isOpen={this.state.col2}
                        className="accordion-collapse"
                      >
                        <div className="accordion-body">
                          <Form>
                            <Label
                              style={{
                                fontSize: 16,
                                alignItems: "center",
                                fontWeight: "bolder",
                                paddingLeft: 24,
                              }}
                            >
                              การเปลี่ยนแปลงหรือเพิ่มอาคารมีวิศวกรผู้ออกแบบหรือให้คำรับรองเกี่ยวกับ
                              ความมั่นคงแข็งแรงของอาคารโรงงาน
                            </Label>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    ชื่อ
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    ที่อยู่/สำนักงาน
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    โทรศัพท์
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    ประเภท
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    สาขา
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    ใบอนุญาตเลขทะเบียน
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="number"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    สิ้นอายุ
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                  ทั้งนี้
                                  การเปลี่ยนแปลงหรืออาคารที่เพิ่มขัดต่อกฎกระทรวงโรงงานหรือไม่
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12}>
                                <Col
                                  md={3}
                                  style={{ marginLeft: 200, marginTop: 10 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    ขัด
                                  </Label>
                                </Col>
                                <Col md={3} style={{ marginTop: 10 }}>
                                  <Input
                                    type="radio"
                                    id="radio-button 2"
                                    name="button"
                                    value="ไม่ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    ไม่ขัด
                                  </Label>
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col md={3}>
                                  <Label
                                    style={{ fontSize: 14, marginLeft: 35 }}
                                  >
                                    เนื่องจาก...
                                  </Label>
                                </Col>
                                <Col md={7}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="โปรดระบุข้อมูล"
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                            <FormGroup>
                              <Row md={12} style={{ paddingLeft: 38 }}>
                                <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                  ความเห็นต่อทำเลสถานที่ตั้ง
                                </Col>
                                <p />
                                <Col md={10} style={{ paddingLeft: 50 }}>
                                  <Input
                                    type="text"
                                    className="form-control"
                                    placeholder="แสดงความคิดเห็น..."
                                    style={{ paddingBottom: 40 }}
                                  />
                                </Col>
                              </Row>
                            </FormGroup>
                            <p />
                          </Form>
                        </div>
                      </Collapse>
                    </div>
                  </div>
                </Col>

                {/* end */}
                <Row
                  className="d-flex justify-content-end"
                  style={{ marginBottom: 87 }}
                >
                  <div className="d-flex justify-content-center">
                    <button
                      type="submit"
                      className="btn btn-style btn-color"
                      style={{ fontSize: 16, fontWeight: "bolder" }}
                    >
                      บันทึก
                    </button>
                  </div>
                </Row>
              </Row>
            </Card>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default UiTabsAccordions;
