import React, { Component } from "react";
import MetaTags from "react-meta-tags";
import {
  Card,
  Col,
  Collapse,
  Container,
  Row,
  Label,
  Form,
  FormGroup,
  Input,
  Progress,
} from "reactstrap";
import { Link } from "react-router-dom";

import Days from "../../../src/theme_components/Common/Date";
import Info from "../../theme_components/Icon/info.png";
import Sheet from "../../theme_components/Icon/sheet.png";
import Factory from "../../theme_components/Icon/factory.png";
import f2building1 from "../../theme_components/Icon/f2building1.png";
import f2building2 from "../../theme_components/Icon/f2building2.png";
import f3ew from "../../theme_components/Icon/f3ew.png";
import f3change from "../../theme_components/Icon/f3change.png";
import f4one from "../../theme_components/Icon/f4one.png";
import f4two from "../../theme_components/Icon/f4two.png";
import f4three from "../../theme_components/Icon/f4three.png";
import f4four from "../../theme_components/Icon/f4four.png";
import f5 from "../../theme_components/Icon/f5.png";
import f6 from "../../theme_components/Icon/f6.png";

// Editable
import Table from "../Tables/EditableTables";

class UiTabsAccordions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // 1st
      col1_1: true,
      col1_2: false,
      col1_3: false,
      //   2nd
      col2_1: true,
      col2_2: false,
      //   3th
      col3_1: true,
      col3_2: false,
      //   4th
      col4_1: true,
      col4_2: false,
      col4_3: false,
      col4_4: false,
      //   5th
      col5_1: true,
      // 6th
      col6_1: true,

      value: 0,

      violet: "#682e77",

      green: "#a0b671",

      grey: "#bcbcbc",

      color1: "#bcbcbc",
      color2: "#bcbcbc",
      color3: "#bcbcbc",

      //  Set page to show first when page is refreshed
      page: 1,
      progress_colors: ["#682e77", "", "", "", "", ""],
    };

    // first
    this.t_col1_1 = this.t_col1_1.bind(this);
    this.t_col1_2 = this.t_col1_2.bind(this);
    this.t_col1_3 = this.t_col1_3.bind(this);
    // Second
    this.t_col2_1 = this.t_col2_1.bind(this);
    this.t_col2_2 = this.t_col2_2.bind(this);
    // Third
    this.t_col3_1 = this.t_col3_1.bind(this);
    this.t_col3_2 = this.t_col3_2.bind(this);
    // Forth
    this.t_col4_1 = this.t_col4_1.bind(this);
    this.t_col4_2 = this.t_col4_2.bind(this);
    this.t_col4_3 = this.t_col4_3.bind(this);
    this.t_col4_4 = this.t_col4_4.bind(this);
    // Fifth
    this.t_col5_1 = this.t_col5_1.bind(this);
    // Sixth
    this.t_col6_1 = this.t_col6_1.bind(this);
    // To set handle change page
    this.handleChangePage = this.handleChangePage.bind(this);

    this.changeValue2 = this.changeValue2.bind(this);
    this.changeColor = this.changeColor.bind(this);
  }
  // 1st
  t_col1_1() {
    this.setState({
      col1_1: !this.state.col1_1,
    });
  }

  t_col1_2() {
    this.setState({
      col1_2: !this.state.col1_2,
    });
  }

  t_col1_3() {
    this.setState({
      col1_3: !this.state.col1_3,
    });
  }
  // 2nd
  t_col2_1() {
    this.setState({
      col2_1: !this.state.col2_1,
    });
  }

  t_col2_2() {
    this.setState({
      col2_2: !this.state.col2_2,
    });
  }
  // 3th
  t_col3_1() {
    this.setState({
      col3_1: !this.state.col3_1,
    });
  }

  t_col3_2() {
    this.setState({
      col3_2: !this.state.col3_2,
    });
  }
  // 4th
  t_col4_1() {
    this.setState({
      col4_1: !this.state.col4_1,
    });
  }

  t_col4_2() {
    this.setState({
      col4_2: !this.state.col4_2,
    });
  }

  t_col4_3() {
    this.setState({
      col4_3: !this.state.col4_3,
    });
  }

  t_col4_4() {
    this.setState({
      col4_4: !this.state.col4_4,
    });
  }
  //   5th
  t_col5_1() {
    this.setState({
      col5_1: !this.state.col5_1,
    });
  }
  //   6th
  t_col6_1() {
    this.setState({
      col6_1: !this.state.col6_1,
    });
  }

  changeValue2 = () => {
    this.setState({
      value: 1,
    });
  };

  changeColor = () => {
    this.setState({
      violet: "#682e77",
    });
  };

  //   Changing Page
  handleChangePage = (page) => (e) => {
    // Changing state
    var progress_colors = ["", "", "", "", "", ""];
    for (let i = 0; i <= page - 1; i++) {
      progress_colors[i] = "#682e77";
    }
    this.setState({
      page: page,
      progress_colors: progress_colors,
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="page-content">
          <MetaTags>
            <title>
              Eform - form3 | Skote - React Admin & Dashboard Template
            </title>
          </MetaTags>

          {/* Page header */}

          <Container md={12}>
            <Row md={12}>
              <Col md={2} className="d-flex justify-content-start">
                <Link to="/" className="back">
                  <i
                    className="bx bx-left-arrow-alt bx-md me-2 "
                    style={{ color: "#A7A7A7" }}
                  />
                </Link>
                <Link to="/" className="back">
                  <span>
                    <Label
                      style={{
                        paddingTop: 1,
                        fontSize: 20,
                        color: "#A7A7A7",
                      }}
                    >
                      ย้อนกลับ
                    </Label>
                  </span>
                </Link>
              </Col>
              {/*  space  */}
              <Col md={10} className="d-flex justify-content-end">
                <Label
                  style={{
                    fontSize: 20,
                    fontWeight: "bolder",
                  }}
                >
                  รายงานผลการตรวจและพิจารณาการขอรับใบอนุญาตขยายโรงงาน
                </Label>
              </Col>
            </Row>
          </Container>

          {/* Ending Page header */}

          <Container
            md={12}
            className=" d-flex "
            style={{ paddingTop: 40, marginBottom: 50 }}
          >
            <Col md="12" md={{ size: 6, offset: 3 }}>
              <div className="mt-5 d-flex justify-content-center">
                <Row md={12} className="position-relative ">
                  <Progress
                    max="6"
                    value={this.state.value}
                    color="success"
                    className="clr-line"
                    style={{ height: "3px" }}
                  ></Progress>
                  <Col md={2}>
                    <button
                      className="position-relative top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                      style={{
                        width: "2.2rem",
                        height: "2.2rem",
                        backgroundColor: this.state.progress_colors[0],
                      }}
                      onClick={this.handleChangePage(1)}
                    >
                      1
                    </button>
                  </Col>

                  <Col md={2}>
                    <button
                      className="position-relative top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                      style={{
                        width: "2.2rem",
                        height: "2.2rem",
                        backgroundColor: this.state.progress_colors[1],
                      }}
                      onClick={this.handleChangePage(2)}
                    >
                      2
                    </button>
                  </Col>
                  <Col md={2}>
                    <button
                      className="position-relatives top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                      style={{
                        width: "2.2rem",
                        height: "2.2rem",
                        backgroundColor: this.state.progress_colors[2],
                      }}
                      onClick={this.handleChangePage(3)}
                    >
                      3
                    </button>
                  </Col>
                  <Col md={2}>
                    <button
                      className="position-relative top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                      style={{
                        width: "2.2rem",
                        height: "2.2rem",
                        backgroundColor: this.state.progress_colors[3],
                      }}
                      onClick={this.handleChangePage(4)}
                    >
                      4
                    </button>
                  </Col>
                  <Col md={2}>
                    <button
                      className="position-relatives top-0 progress-mr translate-middle btn btn-sm btn-grey rounded-pill"
                      style={{
                        width: "2.2rem",
                        height: "2.2rem",
                        backgroundColor: this.state.progress_colors[4],
                      }}
                      onClick={this.handleChangePage(5)}
                    >
                      5
                    </button>
                  </Col>
                  <Col md={2}>
                    <button
                      className="position-relatives top-0  translate-middle btn btn-sm btn-grey rounded-pill"
                      style={{
                        width: "2.2rem",
                        height: "2.2rem",
                        backgroundColor: this.state.progress_colors[5],
                      }}
                      onClick={this.handleChangePage(6)}
                    >
                      6
                    </button>
                  </Col>
                </Row>
              </div>
            </Col>
          </Container>

          {/* Ending Progress Bar */}

          {this.state.page === 1 && (
            <Container md={12}>
              <Card>
                <Row>
                  <Col style={{ marginBottom: 30 }}>
                    <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                      <div className="d-flex justify-content-between">
                        <div>
                          <Label
                            style={{
                              textAlign: "start",
                              fontSize: 18,
                              color: "#717171",
                              paddingLeft: 20,
                            }}
                          >
                            Form 1
                          </Label>
                        </div>
                        <div>
                          <Label
                            style={{
                              fontSize: 18,
                              color: "#717171",
                              paddingRight: 20,
                            }}
                          >
                            วันที่ตรวจ <Days />
                          </Label>
                        </div>
                      </div>
                      <Col
                        md={6}
                        style={{
                          textAlign: "end",
                          fontSize: 25,
                          fontWeight: "bold",
                        }}
                      >
                        ทะเบียนโรงงานเลขที่
                      </Col>
                      <Col md={3}>
                        <Input
                          type="Text"
                          className="form-control mb-4"
                          id="horizontal-number-Input"
                          placeholder="ระบุเลขทะเบียนโรงงาน"
                          style={{
                            textAlign: "center",
                            fontSize: 16,
                          }}
                        ></Input>
                      </Col>
                    </Row>

                    {/* First Accordion */}

                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col1_1}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={Info}
                              alt="Icon"
                            />
                            ข้อมูลทั่วไป
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col1_1}
                          className="accordion-collapse"
                        >
                          <div
                            className="accordion-body"
                            style={{ background: "#F8F8FB" }}
                          >
                            <Form>
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ชื่อผู้ขออนุญาต
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      เบอร์โทรศัพท์
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ชื่อโรงงาน
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ที่ตั้ง
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ประกอบกิจการ
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      กำลังเครื่องจักรเพิ่มขึ้น
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      รวมเป็น
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      จำนวนคนงานเพิ่มขึ้น
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      รวมเป็น
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 30 }}>
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      นโยบาย อก. ที่เกี่ยวข้อง
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12}>
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        fontSize: 14,
                                        paddingLeft: 50,
                                      }}
                                    >
                                      ขัดกับนโยบายหรือไม่
                                    </Label>
                                  </Col>

                                  <Col
                                    md={3}
                                    style={{ marginLeft: 25, marginTop: 10 }}
                                  >
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      ขัด
                                    </Label>
                                  </Col>
                                  <Col md={3} style={{ marginTop: 10 }}>
                                    <Input
                                      type="radio"
                                      id="radio-button 2"
                                      name="button"
                                      value="ไม่ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      ไม่ขัด
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      เนื่องจาก...
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>

                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ข้อมูลอื่นๆ
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 2 */}

                  <Col md={12} style={{ marginBottom: 30 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col1_2}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={Sheet}
                              alt="Icon"
                            />
                            เอกสารประกอบคำขอ
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col1_2}
                          className="accordion-collapse"
                        >
                          <div
                            className="accordion-body"
                            style={{ background: "#F8F8FB" }}
                          >
                            <Label
                              style={{
                                fontSize: 18,
                                fontWeight: "bolder",
                                paddingLeft: 24,
                              }}
                            >
                              เอกสารประกอบคำขอ
                            </Label>
                            <Form>
                              <FormGroup className="row mb-12">
                                <label
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button1"
                                    value="ขัด1"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    สำเนาทะเบียนบ้าน/บัตรประชาชน
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 2"
                                    name="button2"
                                    value="ขัด2"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    สำเนาหนังสือรับรองการจดทะเบียนนิติบุคคล
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  htmlFor="horizontal-email-Input"
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button3"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    แผนผังแสดงสิ่งปลูกสร้างภายในบริเวณโรงงาน
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  htmlFor="horizontal-email-Input"
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button4"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    แผนผังการติดตั้งเครื่องจักร
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  htmlFor="horizontal-email-Input"
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button5"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    แบบแปลนอาคารโรงงาน
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  htmlFor="horizontal-email-Input"
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button6"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    แบบแปลนแผนผังและคำอธิบาย
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  htmlFor="horizontal-email-Input"
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button7"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    เหตุอันตราย หรือก่อให้เกิดความรำคาญ
                                  </Label>
                                </label>
                              </FormGroup>
                              <FormGroup className="row mb-12">
                                <label
                                  htmlFor="horizontal-email-Input"
                                  className="col-sm-12 col-form-label"
                                  style={{ fontSize: 14, paddingLeft: 50 }}
                                >
                                  <Input
                                    type="radio"
                                    id="radio-button 1"
                                    name="button8"
                                    value="ขัด"
                                  ></Input>

                                  <Label
                                    for="ข้อมูลทั่วไป"
                                    style={{ fontSize: 14, paddingLeft: 20 }}
                                  >
                                    การควบคุมการปล่อยมลพิษ
                                  </Label>
                                </label>
                              </FormGroup>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                อื่นๆ
                              </Label>
                              <FormGroup className="row mb-1">
                                <Row>
                                  <Col
                                    className="col-md-1 col-form-label"
                                    style={{ fontSize: 14, paddingLeft: 50 }}
                                  >
                                    <Input
                                      type="radio"
                                      id="ข้อมูลทั่วไป"
                                      name="อื่นๆ 1"
                                      value="ข้อมูลทั่วไป"
                                    ></Input>
                                  </Col>
                                  <Col md={4}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup className="row mb-1">
                                <Row>
                                  <Col
                                    className="col-md-1 col-form-label"
                                    style={{ fontSize: 14, paddingLeft: 50 }}
                                  >
                                    <Input
                                      type="radio"
                                      id="ข้อมูลทั่วไป"
                                      name="อื่นๆ 2"
                                      value="ข้อมูลทั่วไป"
                                    ></Input>
                                  </Col>
                                  <Col md={4}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup className="row mb-1">
                                <Row>
                                  <Col
                                    className="col-md-1 col-form-label"
                                    style={{ fontSize: 14, paddingLeft: 50 }}
                                  >
                                    <Input
                                      type="radio"
                                      id="ข้อมูลทั่วไป"
                                      name="อื่นๆ 3"
                                      value="ข้อมูลทั่วไป"
                                    ></Input>
                                  </Col>
                                  <Col md={4}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 3 */}

                  <Col md={12} style={{ marginBottom: 50 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingThree">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col1_3}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={Factory}
                              alt="Icon"
                            />
                            ที่ตั้งและสภาพแวดล้อมของโรงงาน
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col1_3}
                          className="accordion-collapse"
                        >
                          <div
                            className="accordion-body"
                            style={{ background: "#F8F8FB" }}
                          >
                            <Form>
                              <Label
                                style={{
                                  fontSize: 14,
                                  fontWeight: "bolder",
                                  color: "red",
                                  paddingLeft: 24,
                                }}
                              >
                                *บริเวณโดยรอบและสภาพที่ดินที่ตั้งโรงงาน
                                รวมทั้งข้อกำหนดเกี่ยวกับพื้นที่ตั้งโรงงาน
                              </Label>
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 50 }}>
                                  <Col md={6}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      เป็นไปตามสภาพที่ได้รับอนุญาตให้ประกอบกิจการโรงงาน
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 50 }}>
                                  <Col md={2}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      มีการเปลี่ยนแปลง
                                    </Label>
                                  </Col>
                                  <Col md={6} style={{ marginLeft: 73 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      อยู่ในเขตผังเมือง
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      สี
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ที่ดินหมายเลข
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ประเภทของที่ดิน
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup className="mb-1">
                                <Row
                                  md={12}
                                  style={{ fontSize: 14, paddingLeft: 30 }}
                                >
                                  <Col md={3}>
                                    <Label
                                      className="col-form-label"
                                      style={{
                                        paddingLeft: 20,
                                        alignItems: "center",
                                        fontSize: 14,
                                      }}
                                    >
                                      ทำเลโดยรอบ...เป็น
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={4} style={{ marginLeft: 38 }}>
                                  <Col className="col=md-3">
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      ขัด
                                    </Label>
                                  </Col>
                                  <Col className="col-md-3">
                                    <Input
                                      type="radio"
                                      id="radio-button 2"
                                      name="button"
                                      value="ไม่ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      ไม่ขัด
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12}>
                                  <Col md={11} style={{ marginLeft: 40 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      id="horizontal-email-Input"
                                      placeholder="เนื่องจาก..."
                                      style={{ paddingBottom: 40 }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row>
                                  <Label
                                    className="col-md-12 col-form-label"
                                    style={{ fontSize: 14, marginLeft: 40 }}
                                  >
                                    ความเห็นต่อทำเลสถานที่ตั้ง
                                  </Label>
                                </Row>
                                <Row md={12}>
                                  <Col md={11} style={{ marginLeft: 40 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      id="horizontal-email-Input"
                                      placeholder="แสดงความเห็น"
                                      style={{ paddingBottom: 40 }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* end */}
                  <Row className="d-flex justify-content-end mb-87">
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn btn-style btn-color"
                        style={{ fontSize: 16, fontWeight: "bolder" }}
                        onClick={this.handleChangePage(2)}
                      >
                        บันทึก
                      </button>
                    </div>
                  </Row>
                </Row>
              </Card>
            </Container>
          )}

          {/* This is Form2 */}
          {/* This is Form2 */}
          {/* This is Form2 */}
          {/* This is Form2 */}
          {/* This is Form2 */}
          {/* This is Form2 */}
          {/* This is Form2 */}

          {this.state.page === 2 && (
            <Container md={12}>
              <Card>
                <Row>
                  <Col md={12} style={{ marginBottom: 30 }}>
                    <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                      <div className="d-flex justify-content-between">
                        <div>
                          <Label
                            style={{
                              textAlign: "start",
                              fontSize: 18,
                              color: "#717171",
                              paddingLeft: 20,
                            }}
                          >
                            Form 2
                          </Label>
                        </div>
                        <div>
                          <Label
                            style={{
                              fontSize: 18,
                              color: "#717171",
                              paddingRight: 20,
                            }}
                          >
                            วันที่ตรวจ <Days />
                          </Label>
                        </div>
                      </div>
                      <Container className="d-flex jusstify-content-center">
                        <Col
                          md={12}
                          style={{
                            textAlign: "center",
                            fontSize: 25,
                            fontWeight: "bold",
                          }}
                        >
                          ลักษณะอาคารและบริเวณภายในโรงงาน
                        </Col>
                      </Container>
                    </Row>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col2_1}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={f2building1}
                              alt="Icon"
                            />
                            ข้อมูลทั่วไป
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col2_1}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                อาคาร
                              </Label>

                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={12}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      เป็นไปตามสภาพที่ได้รับอนุญาตให้ประกอบกิจการโรงงาน
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      มีการเปลี่ยนแปลง
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      เพิ่มอาคาร
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ลักษณะ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 2 */}

                  <Col md={12} style={{ marginBottom: 50 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col2_2}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-3"
                              src={f2building2}
                              alt="Icon"
                            />
                            เอกสารประกอบคำขอ
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col2_2}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                การเปลี่ยนแปลงหรือเพิ่มอาคารมีวิศวกรผู้ออกแบบหรือให้คำรับรองเกี่ยวกับ
                                ความมั่นคงแข็งแรงของอาคารโรงงาน
                              </Label>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ชื่อ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ที่อยู่/สำนักงาน
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      โทรศัพท์
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ประเภท
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สาขา
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ใบอนุญาตเลขทะเบียน
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="number"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สิ้นอายุ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                    ทั้งนี้
                                    การเปลี่ยนแปลงหรืออาคารที่เพิ่มขัดต่อกฎกระทรวงโรงงานหรือไม่
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12}>
                                  <Col
                                    md={3}
                                    style={{ marginLeft: 200, marginTop: 10 }}
                                  >
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      ขัด
                                    </Label>
                                  </Col>
                                  <Col md={3} style={{ marginTop: 10 }}>
                                    <Input
                                      type="radio"
                                      id="radio-button 2"
                                      name="button"
                                      value="ไม่ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 14, paddingLeft: 20 }}
                                    >
                                      ไม่ขัด
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      เนื่องจาก...
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                    ความเห็นต่อทำเลสถานที่ตั้ง
                                  </Col>
                                  <p />
                                  <Col md={10} style={{ paddingLeft: 50 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="แสดงความคิดเห็น..."
                                      style={{ paddingBottom: 40 }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* end */}
                  <Row
                    className="d-flex justify-content-end"
                    style={{ marginBottom: 87 }}
                  >
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn btn-style btn-color"
                        style={{ fontSize: 16, fontWeight: "bolder" }}
                        onClick={this.handleChangePage(3)}
                      >
                        บันทึก
                      </button>
                    </div>
                  </Row>
                </Row>
              </Card>
            </Container>
          )}

          {/*this is Form 3 */}
          {/*this is Form 3 */}
          {/*this is Form 3 */}
          {/*this is Form 3 */}
          {/*this is Form 3 */}
          {/*this is Form 3 */}

          {this.state.page === 3 && (
            <Container md={12}>
              <Card>
                <Row>
                  <Col md={12} style={{ marginBottom: 30 }}>
                    <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                      <div className="d-flex justify-content-between">
                        <div>
                          <Label
                            style={{
                              textAlign: "start",
                              fontSize: 18,
                              color: "#717171",
                              paddingLeft: 20,
                            }}
                          >
                            Form 3
                          </Label>
                        </div>
                        <div>
                          <Label
                            style={{
                              fontSize: 18,
                              color: "#717171",
                              paddingRight: 20,
                            }}
                          >
                            วันที่ตรวจ <Days />
                          </Label>
                        </div>
                      </div>
                      <Container className="d-flex jusstify-content-center">
                        <Col
                          md={12}
                          style={{
                            textAlign: "center",
                            fontSize: 25,
                            fontWeight: "bold",
                          }}
                        >
                          ลักษณะอาคารและบริเวณภายในโรงงาน
                        </Col>
                      </Container>
                    </Row>

                    {/* First Accordion */}
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col3_1}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={f3ew}
                              alt="Icon"
                            />
                            เครื่องจักรที่อาจก่อเหตุเดือดร้อนรำคาญ ความเสียหาย
                            หรืออันตราย
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col3_1}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Label style={{ fontSize: 14 }}>
                                    เครื่องจักรที่อาจก่อเหตุเดือดร้อนรำคาญ
                                    ความเสียหาย หรืออันตราย ได้แก่
                                  </Label>
                                  <p />
                                  <Col md={11} style={{ paddingLeft: 20 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                      style={{ paddingBottom: 40 }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 14 }}>
                                    โดยมีการป้องกัน คือ
                                  </Col>
                                  <p />
                                  <Col md={11} style={{ paddingLeft: 20 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                      style={{ paddingBottom: 40 }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 2 */}

                  <Col md={12} style={{ marginBottom: 50 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col3_2}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-3"
                              src={f3change}
                              alt="Icon"
                            />
                            การเปลี่ยนแปลงเครื่องจักรหรือเพิ่มเครื่องจักร
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col3_2}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                การเปลี่ยนแปลงเครื่องจักรหรือเพิ่มเครื่องจักรมีวิศวกรผู้ออกแบบหรือให้คำรับรอง
                                เกี่ยวกับการติดตั้งวางผังเครื่องจักรในโรงงาน
                              </Label>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ชื่อ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ที่อยู่/สำนักงาน
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      โทรศัพท์
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ประเภท
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สาขา
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ใบอนุญาตเลขทะเบียน
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="number"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สิ้นอายุ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* end */}
                  <Row
                    className="d-flex justify-content-end"
                    style={{ marginBottom: 87 }}
                  >
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn btn-style btn-color"
                        style={{ fontSize: 16, fontWeight: "bolder" }}
                        onClick={this.handleChangePage(4)}
                      >
                        บันทึก
                      </button>
                    </div>
                  </Row>
                </Row>
              </Card>
            </Container>
          )}

          {/* This is form 4 */}
          {/* This is form 4 */}
          {/* This is form 4 */}
          {/* This is form 4 */}
          {/* This is form 4 */}

          {this.state.page === 4 && (
            <Container md={12}>
              <Card>
                <Row>
                  <Col md={12} style={{ marginBottom: 30 }}>
                    <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                      <div className="d-flex justify-content-between">
                        <div>
                          <Label
                            style={{
                              textAlign: "start",
                              fontSize: 18,
                              color: "#717171",
                              paddingLeft: 20,
                            }}
                          >
                            Form 4
                          </Label>
                        </div>
                        <div>
                          <Label
                            style={{
                              fontSize: 18,
                              color: "#717171",
                              paddingRight: 20,
                            }}
                          >
                            วันที่ตรวจ <Days />
                          </Label>
                        </div>
                      </div>
                      <Container className="d-flex jusstify-content-center">
                        <Col
                          md={12}
                          style={{
                            textAlign: "center",
                            fontSize: 25,
                            fontWeight: "bold",
                          }}
                        >
                          การควบคุมมลพิษที่จะมีผลกระทบต่อสิ่งแวดล้อม
                        </Col>
                      </Container>
                    </Row>

                    {/* First Accordion */}

                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col4_1}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={f4one}
                              alt="Icon"
                            />
                            สิ่งปฏิกูลหรือวัสดุที่ไม่ใช้แล้วส่วนขยาย
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col4_1}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  marginTop: 30,
                                  paddingLeft: 24,
                                }}
                              >
                                สิ่งปฏิกูลหรือวัสดุที่ไม่ใช้แล้วส่วนขยาย
                              </Label>

                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ paddingLeft: 38, marginTop: 25 }}
                                >
                                  <Col md={12}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ไม่มี หรือไม่น้ำทิ้งเพิ่ม"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      ไม่มี
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ paddingLeft: 38, marginTop: 25 }}
                                >
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      เพิ่มปริมาณ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row
                                  md={12}
                                  style={{ paddingLeft: 38, marginTop: 25 }}
                                >
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      เพิ่มชนิด
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <FormGroup>
                                <Row
                                  md={12}
                                  className="d-flex justify-content-center"
                                >
                                  <Col>
                                    {/* Imported table */}
                                    <Table />
                                    {/* Imported table */}
                                  </Col>
                                </Row>
                              </FormGroup>
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 2 */}

                  <Col md={12} style={{ marginBottom: 30 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col4_2}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-3"
                              src={f4two}
                              alt="Icon"
                            />
                            มลพิษทางน้ำส่วนขยาย
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col4_2}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                มลพิษทางน้ำส่วนขยาย
                              </Label>

                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={12}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ไม่มี หรือไม่น้ำทิ้งเพิ่ม"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      ไม่มี หรือไม่น้ำทิ้งเพิ่ม
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      มีน้ำทิ้งเพิ่ม
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      แหล่งที่มาและปริมาณ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ระบบบำบัดน้ำทิ้งเป็นแบบ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ลักษณะน้ำทิ้งที่ใช้ออกแบบ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />

                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                โดยมีวิศวกรผู้ออกแบบหรือให้คำรับโรงการจัดสร้างระบบบำบัด
                              </Label>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ชื่อ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ที่อยู่/สำนักงาน
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      โทรศัพท์
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ประเภท
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สาขา
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ใบอนุญาติ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สิ้นอายุ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 3 */}

                  <Col md={12} style={{ marginBottom: 30 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col4_3}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-3"
                              src={f4three}
                              alt="Icon"
                            />
                            มลพิษทางอากาศส่วนขยาย
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col4_3}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                มลพิษทางอากาศส่วนขยาย
                              </Label>

                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={12}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ไม่มี หรือไม่น้ำทิ้งเพิ่ม"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      ไม่มี
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>

                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={12}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ไม่มี หรือไม่น้ำทิ้งเพิ่ม"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      มี ฝุ่นละอองม เขม่าควันม กลิ่นเหม็นม
                                      ไอสารเคมี
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>

                              <p />

                              <Table />

                              <p />
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                โดยมีวิศวกรผู้ออกแบบหรือให้คำรับโรงการจัดสร้างระบบบำบัด
                              </Label>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ชื่อ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ที่อยู่/สำนักงาน
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      โทรศัพท์
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ประเภท
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สาขา
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      ใบอนุญาติ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{ fontSize: 14, marginLeft: 35 }}
                                    >
                                      สิ้นอายุ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* Accordion ที่ 4 */}

                  <Col md={12} style={{ marginBottom: 50 }}>
                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col4_4}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-3"
                              src={f4four}
                              alt="Icon"
                            />
                            มาตรการป้องกันและแก้ไขผลกระทบกระเทือนต่อคุณภาพสิ่งแวดล้อมฯ
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col4_4}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 18, marginLeft: 35 }}>
                                    รายงานเกี่ยวกับการศึกษาและมาตรการป้องกันและแก้ไขผลกระทบกระเทือนต่อคุณภาพสิ่งแวดล้อมฯ
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12}>
                                  <Col
                                    md={3}
                                    style={{ marginLeft: 80, marginTop: 10 }}
                                  >
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 16, paddingLeft: 15 }}
                                    >
                                      มี
                                    </Label>
                                  </Col>
                                  <Col md={3} style={{ marginTop: 10 }}>
                                    <Input
                                      type="radio"
                                      id="radio-button 2"
                                      name="button"
                                      value="ไม่ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 16, paddingLeft: 15 }}
                                    >
                                      ไม่มี
                                    </Label>
                                  </Col>
                                  <Col md={3} style={{ marginTop: 10 }}>
                                    <Input
                                      type="radio"
                                      id="radio-button 2"
                                      name="button"
                                      value="ไม่ขัด"
                                    ></Input>

                                    <Label
                                      for="ข้อมูลทั่วไป"
                                      style={{ fontSize: 16, paddingLeft: 15 }}
                                    >
                                      ไม่ต้องมี
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 16, marginLeft: 35 }}>
                                    กรณีจัดทำรายการดังกล่าว
                                    สำนักนโยบายและแผนสิ่งแวดล้อมเห็นชอบ
                                  </Col>
                                  <p />
                                  <Col md={10} style={{ paddingLeft: 50 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="เมื่อ"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                    สรุปความเห็นต่อการควบคุมการปล่อยของเสีย ฯลฯ
                                  </Col>
                                  <p />
                                  <Col md={10} style={{ paddingLeft: 50 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                      style={{ paddingBottom: 40 }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* end */}
                  <Row
                    className="d-flex justify-content-end"
                    style={{ marginBottom: 87 }}
                  >
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn btn-style btn-color"
                        style={{ fontSize: 16, fontWeight: "bolder" }}
                        onClick={this.handleChangePage(5)}
                      >
                        บันทึก
                      </button>
                    </div>
                  </Row>
                </Row>
              </Card>
            </Container>
          )}

          {/* This is Form 5 */}
          {/* This is Form 5 */}
          {/* This is Form 5 */}
          {/* This is Form 5 */}
          {/* This is Form 5 */}
          {/* This is Form 5 */}

          {this.state.page === 5 && (
            <Container md={12}>
              <Card>
                <Row>
                  <Col md={12} style={{ marginBottom: 50 }}>
                    <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                      <div className="d-flex justify-content-between">
                        <div>
                          <Label
                            style={{
                              textAlign: "start",
                              fontSize: 18,
                              color: "#717171",
                              paddingLeft: 20,
                            }}
                          >
                            Form 5
                          </Label>
                        </div>
                        <div>
                          <Label
                            style={{
                              fontSize: 18,
                              color: "#717171",
                              paddingRight: 20,
                            }}
                          >
                            วันที่ตรวจ <Days />
                          </Label>
                        </div>
                      </div>
                      <Container className="d-flex jusstify-content-center">
                        <Col
                          md={12}
                          style={{
                            textAlign: "center",
                            fontSize: 25,
                            fontWeight: "bold",
                          }}
                        >
                          วัตถุอันตราย
                        </Col>
                      </Container>
                    </Row>

                    {/* First Accordion */}

                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col5_1}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={f5}
                              alt="Icon"
                            />
                            วัตถุอันตรายส่วนขยาย
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col5_1}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Label
                                style={{
                                  fontSize: 16,
                                  alignItems: "center",
                                  fontWeight: "bolder",
                                  paddingLeft: 24,
                                }}
                              >
                                วัตถุอันตรายส่วนขยาย
                              </Label>
                              <p />
                              <p />

                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={12}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ไม่มี หรือไม่น้ำทิ้งเพิ่ม"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      ไม่มี
                                    </Label>
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      เพิ่มปริมาณ
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col md={3}>
                                    <Input
                                      type="radio"
                                      id="radio-button 1"
                                      name="button"
                                      value="ขัด"
                                    ></Input>

                                    <Label
                                      style={{ fontSize: 14, marginLeft: 20 }}
                                    >
                                      เพิ่มชนิด
                                    </Label>
                                  </Col>
                                  <Col md={7}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="ได้แก่...โปรดระบุ"
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />

                              <Table />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* end */}

                  <Row
                    className="d-flex justify-content-end"
                    style={{ marginBottom: 87 }}
                  >
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn btn-style btn-color"
                        style={{ fontSize: 16, fontWeight: "bolder" }}
                        onClick={this.handleChangePage(6)}
                      >
                        บันทึก
                      </button>
                    </div>
                  </Row>
                </Row>
              </Card>
            </Container>
          )}

          {/* This is Form 6 */}
          {/* This is Form 6 */}
          {/* This is Form 6 */}
          {/* This is Form 6 */}
          {/* This is Form 6 */}
          {/* This is Form 6 */}

          {this.state.page === 6 && (
            <Container md={12}>
              <Card>
                <Row>
                  <Col md={12} style={{ marginBottom: 50 }}>
                    <Row md={12} style={{ marginBottom: 35, marginTop: 20 }}>
                      <div className="d-flex justify-content-between">
                        <div>
                          <Label
                            style={{
                              textAlign: "start",
                              fontSize: 18,
                              color: "#717171",
                              paddingLeft: 20,
                            }}
                          >
                            Form 6
                          </Label>
                        </div>
                        <div>
                          <Label
                            style={{
                              fontSize: 18,
                              color: "#717171",
                              paddingRight: 20,
                            }}
                          >
                            วันที่ตรวจ <Days />
                          </Label>
                        </div>
                      </div>
                      <Container className="d-flex jusstify-content-center">
                        <Col
                          md={12}
                          style={{
                            textAlign: "center",
                            fontSize: 25,
                            fontWeight: "bold",
                          }}
                        >
                          ความเห็นและข้อเสนอแนะ
                        </Col>
                      </Container>
                    </Row>

                    {/* First Accordion */}

                    <div className="accordion" id="accordion">
                      <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                          <button
                            className="accordion-button fw-medium acc-padding"
                            type="button"
                            onClick={this.t_col6_1}
                            style={{
                              cursor: "pointer",
                              backgroundColor: "#682e77",
                              color: "#fff",
                              fontSize: 16,
                            }}
                          >
                            <img
                              className="img-responsive me-4"
                              src={f6}
                              alt="Icon"
                            />
                            ความเห็นและข้อเสนอแนะ
                          </button>
                        </h2>
                        <Collapse
                          isOpen={this.state.col6_1}
                          className="accordion-collapse"
                        >
                          <div className="accordion-body">
                            <Form>
                              <Container style={{ marginLeft: 25 }}>
                                <FormGroup>
                                  <Label
                                    style={{
                                      fontSize: 16,
                                      alignItems: "center",
                                      fontWeight: "bolder",
                                      paddingLeft: 24,
                                    }}
                                  >
                                    เห็นควร
                                  </Label>
                                  <p />
                                  <p />
                                </FormGroup>

                                <FormGroup>
                                  <Row md={12} style={{ paddingLeft: 38 }}>
                                    <Col md={12}>
                                      <Input
                                        type="radio"
                                        id="radio-button 1"
                                        name="button"
                                        value="ไม่มี หรือไม่น้ำทิ้งเพิ่ม"
                                      ></Input>

                                      <Label
                                        style={{
                                          fontSize: 14,
                                          marginLeft: 20,
                                        }}
                                      >
                                        อนุญาต
                                      </Label>
                                    </Col>
                                  </Row>
                                </FormGroup>
                                <p />
                                <FormGroup>
                                  <Row md={12} style={{ paddingLeft: 38 }}>
                                    <Col md={2}>
                                      <Input
                                        type="radio"
                                        id="radio-button 1"
                                        name="button"
                                        value="ขัด"
                                      ></Input>

                                      <Label
                                        style={{
                                          fontSize: 14,
                                          marginLeft: 20,
                                        }}
                                      >
                                        ไม่อนุญาต
                                      </Label>
                                    </Col>
                                    <Col md={7}>
                                      <Input
                                        type="text"
                                        className="form-control"
                                        placeholder="โปรดระบุข้อมูล"
                                        style={{ marginLeft: 66 }}
                                      />
                                    </Col>
                                  </Row>
                                </FormGroup>
                                <p />
                                <FormGroup>
                                  <Row md={12} style={{ paddingLeft: 38 }}>
                                    <Col md={2}>
                                      <Input
                                        type="radio"
                                        id="radio-button 1"
                                        name="button"
                                        value="ขัด"
                                      ></Input>

                                      <Label
                                        style={{
                                          fontSize: 14,
                                          marginLeft: 20,
                                        }}
                                      >
                                        อื่น
                                      </Label>
                                    </Col>
                                    <Col md={7}>
                                      <Input
                                        type="text"
                                        className="form-control"
                                        placeholder="โปรดระบุข้อมูล"
                                        style={{ marginLeft: 66 }}
                                      />
                                    </Col>
                                  </Row>
                                </FormGroup>
                              </Container>
                              <p />

                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 350 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{
                                        fontSize: 14,
                                        marginLeft: 40,
                                      }}
                                    >
                                      ลงชื่อผู้ตรวจ
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder=""
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                    สรุปความเห็นต่อการควบคุมการปล่อยของเสีย ฯลฯ
                                  </Col>
                                  <p />
                                  <Col md={10} style={{ paddingLeft: 50 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                      style={{
                                        marginLeft: 4,
                                        paddingBottom: 40,
                                        maxWidth: "98%",
                                      }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 340 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{
                                        fontSize: 14,
                                        marginLeft: 50,
                                      }}
                                    >
                                      ลงชื่อ
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder=""
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 38 }}>
                                  <Col style={{ fontSize: 14, marginLeft: 35 }}>
                                    สรุปความเห็นต่อการควบคุมการปล่อยของเสีย ฯลฯ
                                  </Col>
                                  <p />
                                  <Col md={10} style={{ paddingLeft: 50 }}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="โปรดระบุข้อมูล"
                                      style={{
                                        marginLeft: 4,
                                        paddingBottom: 40,
                                        maxWidth: "98%",
                                      }}
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                              <FormGroup>
                                <Row md={12} style={{ paddingLeft: 340 }}>
                                  <Col md={3}>
                                    <Label
                                      style={{
                                        fontSize: 14,
                                        marginLeft: 50,
                                      }}
                                    >
                                      ลงชื่อ
                                    </Label>
                                  </Col>
                                  <Col md={6}>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder=""
                                    />
                                  </Col>
                                </Row>
                              </FormGroup>
                              <p />
                            </Form>
                          </div>
                        </Collapse>
                      </div>
                    </div>
                  </Col>

                  {/* end */}

                  <Row
                    className="d-flex justify-content-end"
                    style={{ marginBottom: 87 }}
                  >
                    <div className="d-flex justify-content-center">
                      <button
                        type="submit"
                        className="btn btn-style btn-color"
                        style={{ fontSize: 16, fontWeight: "bolder" }}
                        onClick={this.handleChangePage(1)}
                      >
                        บันทึก
                      </button>
                    </div>
                  </Row>
                </Row>
              </Card>
            </Container>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default UiTabsAccordions;
