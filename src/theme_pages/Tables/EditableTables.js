import React, { Component } from "react";
import MetaTags from "react-meta-tags";

import { Row, Col } from "reactstrap";
// Editable
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from "react-bootstrap-table2-editor";

const products = [
  { ชนิด: 59, เครื่องจักรหรือแหล่งกำเนิด: 1500, ระบบำบัด: 1000 },
  { ชนิด: 34, เครื่องจักรหรือแหล่งกำเนิด: 1900, ระบบำบัด: 1300 },
  { ชนิด: 67, เครื่องจักรหรือแหล่งกำเนิด: 1300, ระบบำบัด: 1300 },
  { ชนิด: 23, เครื่องจักรหรือแหล่งกำเนิด: 1100, ระบบำบัด: 6400 },
  { ชนิด: 78, เครื่องจักรหรือแหล่งกำเนิด: 1400, ระบบำบัด: 4000 },
];

const columns = [
  {
    dataField: "ชนิด",
    text: "ชนิด",
    headerAlign: "center",
    headerStyle: {
      backgroundColor: "#682e77",
      color: "#fff",
    },
  },
  {
    dataField: "เครื่องจักรหรือแหล่งกำเนิด",
    text: "เครื่องจักรหรือแหล่งกำเนิด",
    headerAlign: "center",
    headerStyle: { backgroundColor: "#682e77", color: "#fff" },
  },
  {
    dataField: "ระบบำบัด",
    text: "วิธี/ระบบำบัด",
    headerAlign: "center",
    headerStyle: { backgroundColor: "#682e77", color: "#fff" },
  },
];

class EditableTables extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="page-content">
          <MetaTags>
            <title>
              Editable Table | Skote - React Admin & Dashboard Template
            </title>
          </MetaTags>
          <div className="container-fluid">
            <Row md={12}>
              <Col md={12}>
                <div className="table-responsive">
                  <BootstrapTable
                    keyField="id"
                    data={products}
                    columns={columns}
                    cellEdit={cellEditFactory({ mode: "click" })}
                    rowStyle={{ backgroundColor: "#ccc" }}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default EditableTables;
