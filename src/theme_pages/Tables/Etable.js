import React, { useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
  PaginationListStandalone,
} from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import { Link } from "react-router-dom";
import * as moment from "moment";

import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Row,
  FormGroup,
} from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from "../../theme_components/Common/Breadcrumb";
// import modal
import EcommerceOrdersModal from "../../E-Commerce/OrderModal";

export default function Etable(props) {
  const [isEdit, setIsEdit] = React.useState(false);
  const [viewmodal, setViewmodal] = React.useState(false);
  const [modal, setModal] = React.useState(false);
  const [order, setOrder] = React.useState({});
  const [users, setusers] = useState([]);
  useEffect(() => {
    UsersGet();
  }, []);

  const UsersGet = () => {
    fetch("https://www.mecallapi.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        setusers(result);
      });
  };

  const UpdateUser = (id) => {
    window.location = "/U-page/" + id;
  };

  const UserDelete = (id) => {
    var data = {
      id: id,
    };
    fetch("https://www.mecallapi.com/api/users/delete", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          UsersGet();
        }
      });
  };

  const [EcommerceOrderColumns] = React.useState([
    {
      text: "id",
      dataField: "id",
      sort: true,
    },
    {
      dataField: "avatar",
      text: "Avatar",
      sort: true,
      formatter: (cellContent, users) => (
        <>
          <div className="d-flex gap-3 avatar-sm">
            <img
              src={users.avatar}
              alt=""
              className="img-thumbnail rounded-circle"
            />
          </div>
        </>
      ),
    },
    {
      dataField: "fname",
      text: "First Name",
      sort: true,
    },
    {
      dataField: "lname",
      text: "Last Name",
      sort: true,
    },

    {
      dataField: "username",
      text: "Username",
      sort: true,
    },

    {
      isDummyField: true,
      text: "Action",
      formatter: (cellContent, users) => (
        <>
          <div className="d-flex gap-3">
            <FormGroup className="d-flex justify-content-center">
              <i
                className="bx bx-pencil bx-sm"
                onClick={() => UpdateUser(users.id)}
                style={{ marginRight: 15 }}
              />

              <i
                className="bx bxs-trash bx-sm"
                onClick={() => UserDelete(users.id)}
              />
            </FormGroup>
          </div>
        </>
      ),
    },
  ]);

  useEffect(() => {}, []);

  const toggle = () => {
    setModal(!modal);
  };

  const handleOrderClicks = () => {
    setOrder("");
    setIsEdit(false);
    toggle();
  };

  // eslint-disable-next-line no-unused-vars
  const handleTableChange = (type, { page, searchText }) => {
    const { users } = props;
    setusers({
      users: users.filter((order) =>
        Object.keys(order).some(
          (key) =>
            typeof order[key] === "string" &&
            order[key].toLowerCase().includes(searchText.toLowerCase())
        )
      ),
    });
  };

  const toggleViewModal = () => {
    setViewmodal(!viewmodal);
  };

  /* Insert,Update Delete data */

  const handleDeleteOrder = (deleteOrder) => {
    if (deleteOrder.id !== undefined) {
      setusers(
        users.filter(
          (order) => order.id.toString() !== deleteOrder.id.toString()
        )
      );
      // onPaginationPageChange(1);
    }
  };

  const handleOrderClick = (arg) => {
    const order = arg;
    setOrder({
      id: order.id,
      orderId: order.orderId,
      billingName: order.billingName,
      orderdate: order.orderdate,
      total: order.total,
      paymentStatus: order.paymentStatus,
      paymentMethod: order.paymentMethod,
      badgeclass: order.badgeclass,
    });

    setIsEdit(true);

    toggle();
  };

  /**
   * Handling submit Order on Order form
   */
  const handleValidOrderSubmit = (e, values) => {
    // const { isEdit, order } = this.state;

    if (isEdit) {
      const updateOrder = {
        id: order.id,
        orderId: values.orderId,
        billingName: values.billingName,
        orderdate: values.orderdate,
        total: values.total,
        paymentStatus: values.paymentStatus,
        paymentMethod: values.paymentMethod,
        badgeclass: values.badgeclass,
      };

      console.log(updateOrder);

      // update Order
      setusers(
        users.map((order) =>
          order.id.toString() === updateOrder.id.toString()
            ? { order, ...updateOrder }
            : order
        )
      );
    } else {
      const newOrder = {
        id: Math.floor(Math.random() * (30 - 20)) + 20,
        orderId: values["orderId"],
        billingName: values["billingName"],
        orderdate: values["orderdate"],
        total: values["total"],
        paymentStatus: values["paymentStatus"],
        paymentMethod: values["paymentMethod"],
        badgeclass: values["badgeclass"],
      };

      console.log(newOrder);
      console.log(users);

      // save new Order
      setusers([...users, newOrder]);
    }
    toggle();
  };

  const handleValidDate = (date) => {
    const date1 = moment(new Date(date)).format("DD MMM Y");
    return date1;
  };

  const { SearchBar } = Search;

  // const { isEdit } = isEdit;

  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: users.length, // replace later with size(Order),
    custom: true,
  };

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ];

  const selectRow = {
    mode: "checkbox",
  };

  return (
    <React.Fragment>
      <EcommerceOrdersModal isOpen={viewmodal} toggle={toggleViewModal} />
      <div className="page-content">
        <MetaTags>
          <title>User.CRUD | Skote - React Admin & Dashboard Template</title>
        </MetaTags>
        <Container fluid>
          <Breadcrumbs title="CRUD" breadcrumbItem="users" />
          <Row>
            <Col sm="12">
              <Card>
                <CardBody>
                  <PaginationProvider
                    pagination={paginationFactory(pageOptions || [])}
                    keyField="id"
                    columns={EcommerceOrderColumns || []}
                    data={users || []}
                  >
                    {({ paginationProps, paginationTableProps }) => (
                      <ToolkitProvider
                        keyField="id"
                        data={users}
                        columns={EcommerceOrderColumns || []}
                        bootstrap4
                        search
                      >
                        {(toolkitProps) => (
                          <React.Fragment>
                            <Row className="mb-2">
                              <Col sm="4">
                                <div className="search-box me-2 mb-2 d-inline-block">
                                  <div className="position-relative">
                                    <SearchBar {...toolkitProps.searchProps} />
                                    <i className="bx bx-search-alt search-icon" />
                                  </div>
                                </div>
                              </Col>
                              <Col sm="8">
                                <div className="text-sm-end">
                                  <Link to="/C-page">
                                    <Button
                                      type="button"
                                      color="success"
                                      className="btn-rounded mb-2 me-2"
                                    >
                                      <i className="mdi mdi-plus me-1" /> Add
                                      New User
                                    </Button>
                                  </Link>
                                </div>
                              </Col>
                            </Row>
                            <div className="table-responsive">
                              <BootstrapTable
                                {...toolkitProps.baseProps}
                                {...paginationTableProps}
                                responsive
                                defaultSorted={defaultSorted}
                                bordered={false}
                                striped={false}
                                selectRow={selectRow}
                                classes={
                                  "table align-middle table-nowrap table-check"
                                }
                                headerWrapperClasses={"table-light"}
                              />
                            </div>
                            <div className="pagination pagination-rounded justify-content-end mb-2">
                              <PaginationListStandalone {...paginationProps} />
                            </div>
                          </React.Fragment>
                        )}
                      </ToolkitProvider>
                    )}
                  </PaginationProvider>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
}
